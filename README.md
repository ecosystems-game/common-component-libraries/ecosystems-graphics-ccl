# EcoSystems Graphics Common Component Library

EcoSystems Common Component Library is a member of the EcoSystems game development libraries. CCL's ("Common Component Libraries") are libraries which contains common-use modules for EcoSystems' ECS (Entity-Component-System).

Here in EcoSystems' Graphics CCL you will find common-use components which require the implementation of EcoSystem's Graphics Library. For Example, the Mesh Renderer Component is a component commonly used across many game projects. Because of its reusability, it is put into a collection of other reusable graphics modules which can easily be integrated into any game project.

The CCL's were added on top of the Core libraries instead of build into the core library so we could keep all the core libraries as their own individual components which can be included into project as necessary.

## Using EcoSystems Graphics-CCL

If you wish to make use of the EcoSystems Graphics CCL, we recommend adding it to your project using git submodules.

Example:
```
git submodule add https://gitlab.com/ecosystems-game/common-component-libraries/ecosystems-graphics-ccl.git
```