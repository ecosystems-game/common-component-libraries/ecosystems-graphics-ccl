#pragma once
#define _WIN32_WINNT 0x0501

#ifdef _WIN32
#   ifdef ECOSYSTEMS_EXPORTS
#       define ECO_API __declspec(dllexport)
#   else
#       define ECO_API __declspec(dllimport)
#   endif
# else
# define ECO_API
#endif