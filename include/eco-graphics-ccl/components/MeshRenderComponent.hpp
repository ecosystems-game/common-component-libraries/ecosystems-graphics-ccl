#ifndef ECOSYSTEMS_GRAPHICS_CCL_MESHRENDERCOMPONENT_HPP
#define ECOSYSTEMS_GRAPHICS_CCL_MESHRENDERCOMPONENT_HPP

#include "EcoApiModule.h"

#include <eco-graphics/Model.hpp>
#include <eco-graphics/Device.hpp>
#include <eco-graphics/RenderSystem.hpp>


namespace ecosystems::ccl::graphics::components {
    struct MeshRenderComponent {
        std::unique_ptr<ecosystems::graphics::Model> model;
        ecosystems::graphics::RenderSystem* render_system;

        ECO_API explicit MeshRenderComponent(std::unique_ptr<ecosystems::graphics::Model>&& p_model,
                                             ecosystems::graphics::RenderSystem* p_render_system)
            : model(std::move(p_model)), render_system(p_render_system) {}

        ECO_API void draw(VkCommandBuffer p_command_buffer,
                                  glm::mat4 p_model_matrix,
                                  glm::mat4 p_projection_view) {
            render_system->renderModel(p_command_buffer, p_model_matrix, *model, p_projection_view);
        }
    };
}


#endif //ECOSYSTEMS_GRAPHICS_CCL_MESHRENDERCOMPONENT_HPP
