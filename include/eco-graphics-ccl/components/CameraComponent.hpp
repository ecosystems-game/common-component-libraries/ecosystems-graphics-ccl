#ifndef ECOSYSTEMS_CAMERACOMPONENT_HPP
#define ECOSYSTEMS_CAMERACOMPONENT_HPP

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>


namespace ecosystems::ccl::graphics::components {
    class CameraComponent {
    public:
        glm::mat4 projection_matrix{1.f};
        glm::mat4 view_matrix{1.f};

        void setOrthographicProjection(float p_left, float p_right, float p_top, float p_bottom, float p_near, float p_far) {
            projection_matrix = glm::mat4{1.0f};
            projection_matrix[0][0] = 2.f / (p_right - p_left);
            projection_matrix[1][1] = 2.f / (p_bottom - p_top);
            projection_matrix[2][2] = 1.f / (p_far - p_near);
            projection_matrix[3][0] = -(p_right + p_left) / (p_right - p_left);
            projection_matrix[3][1] = -(p_bottom + p_top) / (p_bottom - p_top);
            projection_matrix[3][2] = -p_near/(p_far - p_near);
        }

        void setPerspectiveProjection(float p_fovy, float p_aspect, float p_near, float p_far){
            assert(glm::abs(p_aspect - std::numeric_limits<float>::epsilon()) > 0.0f);
            const float tanHalfFovy = tan(p_fovy/2.f);
            projection_matrix = glm::mat4{0.0f};
            projection_matrix[0][0] = 1.f / (p_aspect*tanHalfFovy);
            projection_matrix[1][1] = 1.f / (tanHalfFovy);
            projection_matrix[2][2] = p_far / (p_far - p_near);
            projection_matrix[2][3] = 1.f;
            projection_matrix[3][2] = -(p_far * p_near) / (p_far - p_near);
        }

        void setViewDirection(glm::vec3 p_position, glm::vec3 p_direction, glm::vec3 p_up = glm::vec3(0.f, -1.f, 0.f)){
            const glm::vec3 w{glm::normalize(p_direction)};
            const glm::vec3 u{glm::normalize(glm::cross(w, p_up))};
            const glm::vec3 v{glm::cross(w, u)};

            view_matrix = glm::mat4{1.f};
            view_matrix[0][0] = u.x;
            view_matrix[1][0] = u.y;
            view_matrix[2][0] = u.z;
            view_matrix[0][1] = v.x;
            view_matrix[1][1] = v.y;
            view_matrix[2][1] = v.z;
            view_matrix[0][2] = w.x;
            view_matrix[1][2] = w.y;
            view_matrix[2][2] = w.z;
            view_matrix[3][0] = -glm::dot(u, p_position);
            view_matrix[3][1] = -glm::dot(v, p_position);
            view_matrix[3][2] = -glm::dot(w, p_position);
        }

        void setViewTarget(glm::vec3 p_position, glm::vec3 p_target, glm::vec3 p_up = glm::vec3(0.f, -1.f, 0.f)) {
            setViewDirection(p_position, p_target - p_position, p_up);
        }

        void setViewYXZ(glm::vec3 p_position, glm::vec3 p_rotation) {
            const float c3 = glm::cos(p_rotation.z);
            const float s3 = glm::sin(p_rotation.z);
            const float c2 = glm::cos(p_rotation.x);
            const float s2 = glm::sin(p_rotation.x);
            const float c1 = glm::cos(p_rotation.y);
            const float s1 = glm::sin(p_rotation.y);
            const glm::vec3 u{(c1 * c3 + s1 * s2 * s3), (c2 * s3), (c1 * s2 * s3 - c3 * s1)};
            const glm::vec3 v{(c3 * s1 * s2 - c1 * s3), (c2 * c3), (c1 * c3 * s2 + s1 * s3)};
            const glm::vec3 w{(c2 * s1), (-s2), (c1 * c2)};
            view_matrix = glm::mat4{1.f};
            view_matrix[0][0] = u.x;
            view_matrix[1][0] = u.y;
            view_matrix[2][0] = u.z;
            view_matrix[0][1] = v.x;
            view_matrix[1][1] = v.y;
            view_matrix[2][1] = v.z;
            view_matrix[0][2] = w.x;
            view_matrix[1][2] = w.y;
            view_matrix[2][2] = w.z;
            view_matrix[3][0] = -glm::dot(u, p_position);
            view_matrix[3][1] = -glm::dot(v, p_position);
            view_matrix[3][2] = -glm::dot(w, p_position);
        }
    };
}

#endif //ECOSYSTEMS_CAMERACOMPONENT_HPP
