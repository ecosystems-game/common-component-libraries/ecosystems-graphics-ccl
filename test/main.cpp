#include <stdexcept>
#include <iostream>
#include <eco-system/GameLoop.hpp>

#include "include/scenes/TestScene.hpp"


int main() {
    ecosystems::system::GameLoop gameLoop;
    gameLoop.load_scene(std::make_unique<ecosystems::ccl::graphics::test::scenes::TestScene>());

    try {
        return gameLoop.run();
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}