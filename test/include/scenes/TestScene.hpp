#ifndef ECOSYSTEMS_TESTSCENE_HPP
#define ECOSYSTEMS_TESTSCENE_HPP

#include <iostream>
#include <filesystem>
#include <memory>

#include <eco-system/Scene.hpp>
#include <eco-system/Entity.hpp>
#include <eco-system/components/BehaviorComponent.hpp>
#include <eco-system/components/TransformComponent.hpp>

#include <eco-graphics/Model.hpp>
#include <eco-graphics/Renderer.hpp>
#include <eco-graphics/RenderSystem.hpp>

#include <eco-graphics-ccl/components/CameraComponent.hpp>
#include <eco-graphics-ccl/components/MeshRenderComponent.hpp>


namespace ecosystems::ccl::graphics::test::scenes {
    class TestScene : public system::Scene {
    public:
        explicit TestScene() :
                m_render_system(ecosystems::graphics::RenderSystem(m_device, m_renderer.get_swap_chain_render_pass())) {}

        void mounted() override {
            m_camera = createEntity("camera");
            auto& cameraComponent = m_camera.addComponent<components::CameraComponent>();
            auto& transform = m_camera.getComponent<system::components::TransformComponent>();
            transform.translation = {0.f, 0.f, 0.f};

            m_model = createEntity("model");
            const char* modelFile = "data/models/colored_cube.obj";
            m_model.addComponent<components::MeshRenderComponent>(ecosystems::graphics::Model::createModelFromFile(m_device, modelFile), &m_render_system);
            m_model.getComponent<system::components::TransformComponent>().translation = {-.5f, 0.f, -2.5f};
            m_model.getComponent<system::components::TransformComponent>().scale = .5;

            cameraComponent.setViewTarget(transform.translation, m_model.getComponent<system::components::TransformComponent>().translation);
        }

        void update(float p_delta_time) override {
            glfwPollEvents();
            Scene::update(p_delta_time);

            auto& cameraComponent = m_camera.getComponent<components::CameraComponent>();
            auto& cameraTransform = m_camera.getComponent<system::components::TransformComponent>();
            auto projectionView = cameraComponent.projection_matrix * cameraComponent.view_matrix;
            auto& modelTransform = m_model.getComponent<system::components::TransformComponent>();
            float rotationSpeed = 0.25f;
            modelTransform.rotation = glm::vec3(
                    modelTransform.rotation.x+rotationSpeed*p_delta_time,
                    modelTransform.rotation.y+rotationSpeed*p_delta_time,
                    modelTransform.rotation.z+rotationSpeed*p_delta_time);
            auto modelMatrix = modelTransform.mat4();

            if (auto commandBuffer = m_renderer.BeginFrame()) {
                m_renderer.BeginSwapChainRenderPass(commandBuffer);
                m_render_system.bindCommandBuffer(commandBuffer);

                m_registry.view<components::MeshRenderComponent>().each([=](auto p_entity, auto& p_mrc) {
                    p_mrc.draw(commandBuffer, modelMatrix, projectionView);
                });

                m_renderer.EndSwapChainRenderPass(commandBuffer);
                m_renderer.EndFrame();
            }

            cameraComponent.setViewTarget(cameraTransform.translation, modelTransform.translation);
            cameraComponent.setPerspectiveProjection(glm::radians(50.f), m_renderer.get_aspect_ratio(), 0.1, 10.f);
        }
    private:
        static constexpr int WIDTH = 800;
        static constexpr int HEIGHT = 600;

        ecosystems::graphics::RenderWindow m_render_window{WIDTH, HEIGHT, "EcoSystems"};
        ecosystems::graphics::Device m_device{m_render_window};
        ecosystems::graphics::Renderer m_renderer{m_render_window, m_device};
        ecosystems::graphics::RenderSystem m_render_system;
        system::Entity m_model, m_camera;
    };
}

#endif //ECOSYSTEMS_TESTSCENE_HPP
